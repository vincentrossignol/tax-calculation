package com.soat.tax.calculation;

import com.soat.tax.company.AutoCompany;
import com.soat.tax.company.Company;
import com.soat.tax.company.DaoTaxPercentage;
import com.soat.tax.company.SASCompany;
import com.soat.tax.calculation.exceptions.IllegalSalesRevenueException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TaxCalculationImplShould {

    @Mock
    DaoTaxPercentage daoTaxPercentage;

    private Company autoCompany = new AutoCompany("012345", "Soat");
    private Company sasCompany  = new SASCompany("012345", "Soat", "Rue des frigos");

    @Test
    public void return_25_percent_of_sales_revenue_with_an_auto_company() {
        // given
        Company testedCompany = this.autoCompany;
        // when
        TaxCalculation t = new TaxCalculationImpl(daoTaxPercentage);
        Mockito.when(daoTaxPercentage.getPercentage(testedCompany)).thenReturn(0.25f);
        float tax = t.calculateTax(testedCompany, 1000f);
        // then
        verify(daoTaxPercentage, times(1)).getPercentage(testedCompany);
        assertEquals(250f, tax, 0);
    }

    @Test
    public void return_33_percent_of_sales_revenue_with_a_sas_company() {
        // given
        Company testedCompany = this.sasCompany;
        // when
        TaxCalculation t = new TaxCalculationImpl(daoTaxPercentage);
        Mockito.when(daoTaxPercentage.getPercentage(testedCompany)).thenReturn(0.33f);
        float tax = t.calculateTax(testedCompany, 1000f);
        // then
        verify(daoTaxPercentage, times(1)).getPercentage(testedCompany);
        assertEquals(330f, tax, 0);
    }

    @Test(expected = IllegalSalesRevenueException.class)
    public void throw_illegal_sales_revenue_exception() {
        // given
        Company testedCompany = this.autoCompany;
        // when
        TaxCalculation t = new TaxCalculationImpl(daoTaxPercentage);
        t.calculateTax(testedCompany, -1000f);
    }
}