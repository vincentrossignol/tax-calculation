package com.soat.tax.company;

public class DaoTaxPercentage {

    public float getPercentage(Company company) {
        return company.getTaxPercentage();
    }
}
