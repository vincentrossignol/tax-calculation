package com.soat.tax.company;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SASCompany extends Company {
    private String address;

    public SASCompany(){super(null, null);}

    public SASCompany(String siret, String denomination, String address) {
        super(siret, denomination);
        this.address = address;
    }

    @Override
    public float getTaxPercentage() {
        return 0.33f;
    }
}
