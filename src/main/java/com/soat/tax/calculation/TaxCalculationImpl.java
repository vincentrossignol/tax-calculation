package com.soat.tax.calculation;

import com.soat.tax.company.Company;
import com.soat.tax.company.DaoTaxPercentage;
import com.soat.tax.calculation.exceptions.IllegalSalesRevenueException;


public class TaxCalculationImpl implements TaxCalculation {

    TaxCalculationImpl(DaoTaxPercentage daoTaxPercentage) {
        this.daoTaxPercentage = daoTaxPercentage;
    }

    private DaoTaxPercentage daoTaxPercentage;

    @Override
    public float calculateTax(Company company, float salesRevenue) {
        if(salesRevenue < 0){
            throw new IllegalSalesRevenueException();
        }

        if(company == null){
            throw new IllegalArgumentException();
        }

        float percentage = daoTaxPercentage.getPercentage(company);

        return percentage * salesRevenue;
    }
}
