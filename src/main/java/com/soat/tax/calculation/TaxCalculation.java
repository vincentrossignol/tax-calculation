package com.soat.tax.calculation;

import com.soat.tax.company.Company;

public interface TaxCalculation {
    float calculateTax(Company company, float salesRevenue);
}
